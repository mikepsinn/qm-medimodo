# Quantimodo.PostStudyCreateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**study** | [**Study**](Study.md) |  | [optional] 
**status** | **String** | Ex: ok | [optional] 
**success** | **Boolean** | Ex: true | [optional] 
**description** | **String** | Can be used as body of help info popup | [optional] 
**summary** | **String** | Can be used as title in help info popup | [optional] 


