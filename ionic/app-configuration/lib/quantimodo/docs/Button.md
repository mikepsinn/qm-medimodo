# Quantimodo.Button

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**link** | **String** | Ex: https://local.quantimo.do | 
**text** | **String** | Ex: Connect | 
**ionIcon** | **String** | Ex: ion-refresh | [optional] 
**color** | **String** | Ex: #f2f2f2 | [optional] 
**additionalInformation** | **String** | Ex: connect | [optional] 


