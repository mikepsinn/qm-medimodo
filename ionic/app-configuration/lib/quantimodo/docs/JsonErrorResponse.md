# Quantimodo.JsonErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** | Error message | [optional] 
**status** | **String** | Status: \&quot;ok\&quot; or \&quot;error\&quot; | 
**description** | **String** | Can be used as body of help info popup | [optional] 
**summary** | **String** | Can be used as title in help info popup | [optional] 


