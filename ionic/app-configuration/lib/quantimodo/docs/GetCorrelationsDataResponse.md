# Quantimodo.GetCorrelationsDataResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**correlations** | [**[Correlation]**](Correlation.md) |  | 
**explanation** | [**Explanation**](Explanation.md) |  | 
**description** | **String** | Can be used as body of help info popup | [optional] 
**summary** | **String** | Can be used as title in help info popup | [optional] 


