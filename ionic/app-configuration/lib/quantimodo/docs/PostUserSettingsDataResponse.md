# Quantimodo.PostUserSettingsDataResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseId** | **Number** | Ex: 1 | [optional] 
**description** | **String** | Can be used as body of help info popup | [optional] 
**summary** | **String** | Can be used as title in help info popup | [optional] 


