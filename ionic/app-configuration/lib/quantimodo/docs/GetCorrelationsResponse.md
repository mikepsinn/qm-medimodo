# Quantimodo.GetCorrelationsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**GetCorrelationsDataResponse**](GetCorrelationsDataResponse.md) |  | [optional] 
**description** | **String** | Can be used as body of help info popup | 
**summary** | **String** | Can be used as title in help info popup | 


