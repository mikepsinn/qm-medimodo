# Quantimodo.StudyCreationBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**predictorVariableName** | **String** | Name of predictor variable | 
**outcomeVariableName** | **String** | Name of the outcome variable | 
**studyTitle** | **String** | Title of your study (optional) | [optional] 


