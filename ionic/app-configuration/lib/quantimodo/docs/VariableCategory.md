# Quantimodo.VariableCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appType** | **String** | Ex: mood | [optional] 
**causeOnly** | **Boolean** | Ex: false | [optional] 
**combinationOperation** | **String** | Ex: MEAN | [optional] 
**createdTime** | **String** | UTC ISO 8601 YYYY-MM-DDThh:mm:ss | [optional] 
**unitAbbreviatedName** | **String** | Ex: /5 | [optional] 
**unitId** | **Number** | Ex: 10 | [optional] 
**durationOfAction** | **Number** | Ex: 86400 | [optional] 
**fillingValue** | **Number** | Ex: -1 | [optional] 
**helpText** | **String** | Ex: What emotion do you want to rate? | [optional] 
**id** | **Number** | Ex: 1 | [optional] 
**imageUrl** | **String** | Ex: https://maxcdn.icons8.com/Color/PNG/96/Cinema/theatre_mask-96.png | [optional] 
**ionIcon** | **String** | Ex: ion-happy-outline | [optional] 
**manualTracking** | **Boolean** | Ex: true | [optional] 
**maximumAllowedValue** | **String** | Ex:  | [optional] 
**measurementSynonymSingularLowercase** | **String** | Ex: rating | [optional] 
**minimumAllowedValue** | **String** | Ex:  | [optional] 
**moreInfo** | **String** | Ex: Do you have any emotions that fluctuate regularly?  If so, add them so I can try to determine which factors are influencing them. | [optional] 
**name** | **String** | Category name | 
**onsetDelay** | **Number** | Ex: 0 | [optional] 
**outcome** | **Boolean** | Ex: true | [optional] 
**pngPath** | **String** | Ex: img/variable_categories/emotions.png | [optional] 
**pngUrl** | **String** | Ex: https://quantimodo.quantimo.do/ionic/Modo/www/img/variable_categories/emotions.png | [optional] 
**_public** | **Boolean** | Ex: true | [optional] 
**svgPath** | **String** | Ex: img/variable_categories/emotions.svg | [optional] 
**svgUrl** | **String** | Ex: https://quantimodo.quantimo.do/ionic/Modo/www/img/variable_categories/emotions.svg | [optional] 
**updated** | **Number** | Ex: 1 | [optional] 
**updatedTime** | **String** | UTC ISO 8601 YYYY-MM-DDThh:mm:ss | [optional] 
**variableCategoryName** | **String** | Ex: Emotions | [optional] 
**variableCategoryNameSingular** | **String** | Ex: Emotion | [optional] 


