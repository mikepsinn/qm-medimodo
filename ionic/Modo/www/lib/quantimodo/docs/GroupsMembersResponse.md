# Quantimodo.GroupsMembersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupsMembers** | [**[GroupsMember]**](GroupsMember.md) |  | 
**description** | **String** | GroupsMember | [optional] 
**summary** | **String** | GroupsMember | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


