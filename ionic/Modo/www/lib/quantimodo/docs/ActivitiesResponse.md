# Quantimodo.ActivitiesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activities** | [**[Activity]**](Activity.md) |  | 
**description** | **String** | Activity | [optional] 
**summary** | **String** | Activity | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


