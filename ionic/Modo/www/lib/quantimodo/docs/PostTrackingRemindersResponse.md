# Quantimodo.PostTrackingRemindersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**PostTrackingRemindersDataResponse**](PostTrackingRemindersDataResponse.md) |  | [optional] 
**message** | **String** | Message | [optional] 
**status** | **Number** | Status code | 
**success** | **Boolean** |  | 
**description** | **String** | Can be used as body of help info popup | [optional] 
**summary** | **String** | Can be used as title in help info popup | [optional] 


