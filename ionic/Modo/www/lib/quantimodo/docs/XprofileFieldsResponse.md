# Quantimodo.XprofileFieldsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xprofileFields** | [**[XprofileField]**](XprofileField.md) |  | 
**description** | **String** | XprofileField | [optional] 
**summary** | **String** | XprofileField | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


