# Quantimodo.GetSharesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorizedClients** | [**AuthorizedClients**](AuthorizedClients.md) |  | [optional] 
**description** | **String** | Can be used as body of help info popup | 
**summary** | **String** | Can be used as title in help info popup | 


