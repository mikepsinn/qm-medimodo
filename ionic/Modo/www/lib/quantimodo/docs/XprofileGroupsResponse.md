# Quantimodo.XprofileGroupsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xprofileGroups** | [**[XprofileGroup]**](XprofileGroup.md) |  | 
**description** | **String** | XprofileGroup | [optional] 
**summary** | **String** | XprofileGroup | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


