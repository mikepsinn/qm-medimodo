# Quantimodo.FeedResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cards** | [**[Card]**](Card.md) |  | 
**description** | **String** | Tracking reminder notifications, messages, and study result cards that can be displayed in user feed or stream | 
**summary** | **String** | Tracking reminder notifications, messages, and study results | 


