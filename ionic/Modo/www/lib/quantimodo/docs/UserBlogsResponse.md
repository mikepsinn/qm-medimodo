# Quantimodo.UserBlogsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userBlogs** | [**[UserBlog]**](UserBlog.md) |  | 
**description** | **String** | UserBlog | [optional] 
**summary** | **String** | UserBlog | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


