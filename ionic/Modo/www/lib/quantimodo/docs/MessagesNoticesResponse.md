# Quantimodo.MessagesNoticesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messagesNotices** | [**[MessagesNotice]**](MessagesNotice.md) |  | 
**description** | **String** | MessagesNotice | [optional] 
**summary** | **String** | MessagesNotice | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


