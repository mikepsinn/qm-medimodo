# Quantimodo.XprofileDataResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xprofileData** | [**[XprofileDatum]**](XprofileDatum.md) |  | 
**description** | **String** | XprofileDatum | [optional] 
**summary** | **String** | XprofileDatum | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


