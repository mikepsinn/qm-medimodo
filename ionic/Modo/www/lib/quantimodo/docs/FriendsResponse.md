# Quantimodo.FriendsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**friends** | [**[Friend]**](Friend.md) |  | 
**description** | **String** | Friend | [optional] 
**summary** | **String** | Friend | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


