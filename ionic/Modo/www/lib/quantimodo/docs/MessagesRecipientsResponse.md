# Quantimodo.MessagesRecipientsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messagesRecipients** | [**[MessagesRecipient]**](MessagesRecipient.md) |  | 
**description** | **String** | MessagesRecipient | [optional] 
**summary** | **String** | MessagesRecipient | [optional] 
**image** | [**Image**](Image.md) |  | [optional] 
**avatar** | **String** | Square icon png url | [optional] 
**ionIcon** | **String** | Ex: ion-ios-person | [optional] 
**html** | **String** | Embeddable list of study summaries with explanation at the top | [optional] 


