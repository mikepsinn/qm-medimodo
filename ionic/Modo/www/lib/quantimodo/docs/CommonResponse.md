# Quantimodo.CommonResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** | Can be used as body of help info popup | 
**summary** | **String** | Can be used as title in help info popup | 


