# Quantimodo.PostMeasurementsDataResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userVariables** | [**[Variable]**](Variable.md) |  | [optional] 
**description** | **String** | Can be used as body of help info popup | [optional] 
**summary** | **String** | Can be used as title in help info popup | [optional] 


