# Quantimodo.PostTrackingRemindersDataResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trackingReminderNotifications** | [**[TrackingReminderNotification]**](TrackingReminderNotification.md) |  | [optional] 
**trackingReminders** | [**[TrackingReminder]**](TrackingReminder.md) |  | [optional] 
**userVariables** | [**[Variable]**](Variable.md) |  | [optional] 
**description** | **String** | Can be used as body of help info popup | [optional] 
**summary** | **String** | Can be used as title in help info popup | [optional] 


